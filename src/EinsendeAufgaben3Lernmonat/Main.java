package EinsendeAufgaben3Lernmonat;

public class Main {
    public static void main(String[] args) {
/*
        Listenelement2 listenAnfang = new Listenelement2();

        System.out.print("-".repeat(25));
        System.out.print(" AM ENDE DER LISTE HINZUFÜGEN RECURSIVE METHODE ");
        System.out.println("-".repeat(25));
        for(int element=1; element<=5; element++){
            System.out.print("Nachdem wir "+element+" hinzugefügt haben dann wurde: ");
            listenAnfang.addEnde(element);
            listenAnfang.ausgeben();
        }
        System.out.println();


        Listenelement listenRückwärts = new Listenelement();

        System.out.print("-".repeat(25));
        System.out.print(" AM ANFANG DER LISTE HINZUFÜGEN + Rückwärts ausgeben RECURSIVE METHODE ");
        System.out.println("-".repeat(25));
        for(int element=1; element<=5; element++){
            listenRückwärts.addAnfang(element);
        }
        listenRückwärts.ausgebenRückwärts();
        System.out.println();
*/



        int[] numbers = {8, 3, 7, 9, 1, 2, 4};
        sort(numbers);
    }

    public static int smallest(int[] array) {
        int min = 999;

        for (int i = 0; i < array.length; i++) {
            if (array[i] < min) {
                min = array[i];
            }
        }
        return min;

    }

    public static int indexOfSmallest(int[] array) {
        int index = 0;
        for (int i = 0; i < array.length; i++) {
            if (array[i] == smallest(array)) {
                index = i;
            }

        }
        ;
        return index;
    }


    public static int indexOfSmallestFrom(int[] table, int startIndex) {
        int index = startIndex;
        int[] current = new int[table.length - index];

        for (int j = 0; j < current.length; j++) {
            current[j] = table[index];
            index++;

        }

        return startIndex + indexOfSmallest(current);

    }


    public static void swap(int[] array, int index1, int index2) {
        int[] newArray = new int[array.length];
        for (int i = 0; i < array.length; i++) {
            newArray[i] = array[i];
        }
        array[index1] = newArray[index2];
        array[index2] = newArray[index1];
    }


    public static void sort(int[] array) {
        int [] newArray=new int[array.length];
        for(int i=0; i<array.length; i++){
            newArray[i]=array[i];
        }

        for(int i=0; i<array.length; i++){
            array[i]=newArray[indexOfSmallestFrom(newArray, i)];

        }


    }
}
