package javaauf4;
/* ######################################################
Einsendeaufgabe 4
###################################################### */
//verweinbaren von der Klasse Rechnungen in den package javaauf4
public class Rechnungen {

    //ich muss darauf achten, dass die methoden laut Aufgabenstellung Klassenmethoden werden müssen => static
    public static double wurzel(double zahl){
        return Math.sqrt(zahl);

    }
    //Dank polymorphism kann ich die Funktion quadrat mit verschiedenen Argumenten erstellen:
    //zuerst mit int in-/output
    public static double quadrat(double zahl){
        return zahl*zahl;

    }

    //dann mit double in-/output
    public static int quadrat(int zahl){
        return  zahl*zahl;

    }

}
