package ÜbungsAufgaben5Lernmonat.java08d_codes;


import javax.swing.*;

/********************************************************** Eine Klasse für Eingaben über einen grafischen Dialog *********************************************************** Das Einlesen erfolgt über eine überladene Klassenmethode einlesen()
     Die Klassenvariable abgebrochen speichert, ob die
     Eingabe über die Schaltfläche Abbrechen abgebrochen wurde *********************************************************** Beschreibung der Methode einlesen()
     Die Methode liest über die Methode lesen() einen Wert
     ein und versucht, diesen Wert in den passenden Typ umzuwandeln. Die Eingabe wird so lange wiederholt,
     bis ein gültiger Typ eingegeben wurde bzw. auf die Schaltfläche Abbrechen geklickt wurde.
     Parameter: wert für den Typ, der verarbeitet werden soll Rückgabe: Der Wert, der umgewandelt wurde
     Der Typ hängt vom übergebenen Parameter ab
     Die Methode ist dreimal vorhanden (für int, float und double) *********************************************************** Beschreibung der Methode lesen()
     Die Methode wird zum Einlesen der Werte über den Dialog benutzt.
     Sie setzt außerdem die Klassenvariable abgebrochen. Parameter: text als Zeichenkette für die Ausgabe im
     Dialog
     Rückgabe: Der eingelesene Wert als Zeichenkette *********************************************************/ //die Klasse befindet sich im Package eindialog

    //für den Eingabedialog

public class EingabeDialog {

    public EingabeDialog() {
    }

    //die Klassenvariable abgebrochen
    public static boolean abgebrochen;

    private static String lesen(String text) {
        String eingabeTemp;
        eingabeTemp = JOptionPane.showInputDialog("Bitte geben Sie einen " + text + " Wert ein:");
        //Wichtig! abgebrochen muss wieder auf false gesetzt werden, wenn eine Eingabe erfolgt ist
        if (eingabeTemp == null)
            abgebrochen = true;
        else
            abgebrochen = false;
        return (eingabeTemp);
    }


    //die überladene Methode einlesen() für den Typ int
    public static int einlesen(int wert) {
        int wertTemp = 0;
        boolean gelungen = false;
        String eingabeTemp;
        while (gelungen == false) {
            //die Eingabe lesen
            eingabeTemp = lesen("int");
            //wenn nicht auf Abbrechen geklickt wurde,
            //wird versucht, die Eingabe in den Typ int
            //umzuwandeln
            if (abgebrochen == false) {
                try {
                    wertTemp = Integer.parseInt(eingabeTemp);
                    gelungen = true;
                } catch (NumberFormatException e) {
                    //bitte in einer Zeile eingeben
                    //eine Ausgabe über einen grafischen Dialog
                    JOptionPane.showMessageDialog(null, "Ihre Eingabe war nicht gültig. Bitte wiederholen ...");
                }
            }
            //wenn die Eingabe abgebrochen wurde, wird die
            //Schleife direkt wieder beendet
            else
                gelungen = true;
        }
        //den Wert zurückliefern
        //wenn die Eingabe abgebrochen wurde, wird 0 geliefert
        return wertTemp;
    }

    //die überladene Methode einlesen() für den Typ double //Beschreibung siehe oben bzw. bei der Methode für den double-Typ
    public static double einlesen(double wert) {
        double wertTemp = 0;
        boolean gelungen = false;
        String eingabeTemp;
        while (gelungen == false) {
            eingabeTemp = lesen("double");
            if (abgebrochen == false) {
                try {
                    wertTemp = Double.parseDouble(eingabeTemp);
                    gelungen = true;
                } catch (NumberFormatException e) {
                    //bitte in einer Zeile eingeben
                    //eine Ausgabe über einen grafischen Dialog JOptionPane.showMessageDialog(null, "Ihre Eingabe war nicht gültig. Bitte wiederholen ...");
                }
            } else
                gelungen = true;
        }
        return wertTemp;
    }

    //die überladene Methode einlesen() für den Typ float //Beschreibung siehe oben bzw. bei der Methode für den float-Typ
    public static float einlesen(float wert) {
        float wertTemp = 0F;
        boolean gelungen = false;
        String eingabeTemp;
        while (gelungen == false) {
            eingabeTemp = lesen("float");
            if (abgebrochen == false) {
                try {
                    wertTemp = Float.parseFloat(eingabeTemp);
                    gelungen = true;
                } catch (NumberFormatException e) {
                    //bitte in einer Zeile eingeben
                    //eine Ausgabe über einen grafischen Dialog JOptionPane.showMessageDialog(null, "Ihre Eingabe war nicht gültig. Bitte wiederholen ...");
                }
            } else
                gelungen = true;
        }
        return wertTemp;
    }

    public static void main(String[] args) {
        int intWert = 0;
        double doubleWert = 0;
//der Aufruf der Methode einlesen() mit einem int-Typ intWert = EingabeDialog.einlesen(intWert);
        if (EingabeDialog.abgebrochen == true)
            System.out.println("Sie haben die Eingabe abgebrochen.");
        else
            //bitte in einer Zeile eingeben
            System.out.println("Sie haben " + intWert
                    + " eingegeben");
//und nun ein Test mit einem double-Typen
        doubleWert = EingabeDialog.einlesen(doubleWert);
        if (EingabeDialog.abgebrochen == true)
        System.out.println("Sie haben die Eingabe abgebrochen.");
        else

        System.out.println("Sie haben " + doubleWert + " eingegeben");
    }

}