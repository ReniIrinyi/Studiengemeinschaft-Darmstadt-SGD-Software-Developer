/*############################################
 * Ein kleiner Fehler mit großer Auswirkung 
 ###########################################*/ 
package ÜbungsAufgaben5Lernmonat.java08d_codes.java08d_01_01.src.java08d_01_01;

public class Java08d_01_01 {
	public static void main(String[] args) {
		int zaehler = 1;
		while (zaehler < 10) {
			System.out.println("Schleifendurchlauf: " + zaehler);
		}
	}
}
