/*########################
 * die Anweisung finally 
 ########################*/
package ÜbungsAufgaben5Lernmonat.java08d_codes.java08d_05_10.src.java08d_05_10;

public class Java08d_05_10 {
	public static void main(String[] args) {
		//Übungen.Ziege ziege=new Übungen.Ziege(15, (char) 2);

		//ein Array mit 10 Elementen erzeugen
		int testArray[] = new int[10];
		int index = 0;
		//ein ungültiger Zugriff
		try {
			for (;index < 100; index++)
				testArray[index] = index;
		}
		//Ausnahme IndexOutOfBoundsException abfangen
		catch (IndexOutOfBoundsException e) {
			System.out.println("Der Index " + index + " ist nicht gültig.");
		}
		//die Ausgabe erfolgt in jedem Fall
		finally {
			System.out.println("Dieser Text erscheint immer.");
		}
	}
}
