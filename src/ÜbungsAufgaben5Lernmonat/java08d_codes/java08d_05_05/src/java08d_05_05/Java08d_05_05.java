/*###############################################
 * die Klasse erzeugt nur eine neue Instanz
 * der Klasse Ausgabe und ruft dann die Methode
 * aufruf1() auf   
 ###############################################*/
package ÜbungsAufgaben5Lernmonat.java08d_codes.java08d_05_05.src.java08d_05_05;

public class Java08d_05_05 {
	public static void main(String[] args) {
		Ausgabe probe = new Ausgabe();
		probe.aufruf1();
	}
}
