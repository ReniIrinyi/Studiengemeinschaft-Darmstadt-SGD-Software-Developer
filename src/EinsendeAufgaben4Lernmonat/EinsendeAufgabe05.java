package EinsendeAufgaben4Lernmonat;
/* ######################################################
Einsendeaufgabe 5
###################################################### */
public class EinsendeAufgabe05 {
    public static void main(String[] args) {
        Insekt insekt=new Insekt(15,30);
        Biene biene=new Biene(0.1,0.1,30);
        insekt.ausgabe();
        biene.ausgabe();
    }
}

class Insekt {
    double laenge;
    double gewicht;

    //der Konstruktor
    Insekt(double laenge, double gewicht) {
        this.laenge = laenge;
        this.gewicht = gewicht;
    }

    //die Methode zum Essen
    void essen() {
        laenge = laenge + 1;
        gewicht = gewicht + 1;
    }

    //die Methode zur Ausgabe
    void ausgabe() {
        System.out.println("Das Insekt ist " + laenge + " cm lang und wiegt " + gewicht + " Gramm.");
    }
}

class Biene extends Insekt{
int geschwindigkeit;
    public Biene(double laenge, double gewicht, int geschwindigkeit) {
        super(laenge, gewicht);
        this.geschwindigkeit=geschwindigkeit;
    }

    @Override
    void ausgabe(){
        System.out.println("Die Biene ist " + laenge + " cm lang, wiegt " + gewicht + " Gramm, und fliegt "+this.geschwindigkeit+"km/h");
    }
}