package EinsendeAufgaben5Lernmonat;

/* ######################################################
Einsendeaufgabe 6
###################################################### */
/*
Überarbeiten Sie die Plausibilitätsprüfung in dem Konstruktor der Klasse Wein aus dem Code 1.5
dieses Studienhefts so, dass bei ungültigen Werten nicht mehr stillschweigend
Standardwerte gesetzt werden, sondern jeweils eine Ausnahme der Klasse IllegalArgumentException ausgelöst wird.

Sorgen Sie dafür, dass diese Ausnahmen an den Aufrufer weitergeleitet werden. Erstellen Sie
auch eine main()-Methode, in der Sie die Ausnahmen behandeln, und testen Sie dort die Änderungen.

 */
class Plausibilitätsprüfung {
    public static void main(String[] args) {
        //eine neue Instanz für den Wein erzeugen
        Wein2 flasche1 = new Wein2(10,20.5);
        //den Preis ausgeben
        System.out.println(flasche1.getPreisProFlasche());

        //eine neue Instanz mit unsinnigen Werten erzeugen
        Wein2 flasche2 = new Wein2(-10,20.5);
        //den Preis ausgeben
        System.out.println(flasche2.getPreisProFlasche());
    }


}

public class Wein2 {
    private int alter;
    private double grundpreis;
    private double preisProFlasche;

    //der StandardkKonstruktor
    public Wein2() {
        //er ruft über this() den Konstruktor mit den beiden Parametern auf
        //und übergibt die Standardwerte
        this(1, 10);
    }

    //der Konstruktor setzt das Alter
    //der Preis erhält einen Standardwert
    public Wein2(int alter) {
        //er ruft ebenfalls über this den Konstruktor mit den beiden Parametern auf
        this(alter, 10);
    }

    //der Konstruktor setzt den Preis
    //das Alter erhält einen Standardwert
    public Wein2(double grundpreis) {
        //und dieser Konstruktor ruft auch den Konstruktor mit den beiden Parametern auf
        this(1, grundpreis);
    }

    //der Konstruktor setzt das Alter und den Grundpreis
    //er enthält jetzt Plausibilitätsprüfungen
    public Wein2(int alter, double grundpreis) {
        //die Plausibilitätsprüfung für das Alter
        if (alter > 0)
            this.alter = alter;
            //sonst den Standardwert 1 setzen
        else
            this.alter = 1;
        //und auch für den Preis
        if (grundpreis > 9)
            this.grundpreis = grundpreis;
        else
            this.grundpreis = 10;
    }

    //die Methode berechnet den Preis pro Flasche
    public void preisBerechnen() {
        preisProFlasche = alter * grundpreis;
    }

    //die Methode liefert den Preis pro Flasche
    public double getPreisProFlasche() {
        preisBerechnen();
        return preisProFlasche;
    }
}
